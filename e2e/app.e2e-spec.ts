import { SquarePlotterPage } from './app.po';

describe('square-plotter App', () => {
  let page: SquarePlotterPage;

  beforeEach(() => {
    page = new SquarePlotterPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});

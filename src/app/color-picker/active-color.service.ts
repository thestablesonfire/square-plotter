import { Injectable } from '@angular/core';

@Injectable()
export class ActiveColorService {
  activeColor: string;
  emptyColor = 'rgba(0,0,0,0)';

  constructor() {
    this.activeColor = '#FFF';
  }

}

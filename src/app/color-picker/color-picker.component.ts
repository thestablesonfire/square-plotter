import { Component, OnInit } from '@angular/core';
import { ActiveColorService } from './active-color.service';

@Component({
    selector: 'app-color-picker',
    templateUrl: './color-picker.component.html',
    styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {
    activeColor: string;
    colors: string[];

    constructor(private activeColorService: ActiveColorService) {
        this.activeColor = activeColorService.activeColor;
    }

    setActiveColor(color) {
        this.activeColor = color;
        this.activeColorService.activeColor = color;
    }

    ngOnInit() {
        this.colors = [
            '#000',
            '#808080',
            '#A9A9A9',
            '#D3D3D3',
            '#FF0000',
            '#ad4c4c',
            '#ad744c',
            '#4c3423',
            '#fcf0c9',
            '#FFa500',
            '#c17d00',
            '#ffff00',
            '#adab4c',
            '#00FF00',
            '#4cad4d',
            '#00ffff',
            '#0000FF',
            '#4c4cad',
            '#ff00ff',
            '#a04cad',
            '#ffffff'
        ];
        this.setActiveColor(this.colors[0]);
    }

}

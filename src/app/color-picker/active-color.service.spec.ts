import { TestBed, inject } from '@angular/core/testing';

import { ActiveColorService } from './active-color.service';

describe('ActiveColorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActiveColorService]
    });
  });

  it('should be created', inject([ActiveColorService], (service: ActiveColorService) => {
    expect(service).toBeTruthy();
  }));
});

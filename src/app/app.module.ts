import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ActiveColorService } from './color-picker/active-color.service';
import { PlotterComponent } from './plotter/plotter.component';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { ToolPickerComponent } from './tool-picker/tool-picker.component';
import { ActiveToolService } from './tool-picker/active-tool.service';

@NgModule({
  declarations: [
    AppComponent,
    ColorPickerComponent,
    PlotterComponent,
    ToolPickerComponent
  ],
  imports: [
    BrowserModule,
      FormsModule
  ],
  providers: [ActiveColorService, ActiveToolService],
  bootstrap: [AppComponent],
  exports: [PlotterComponent, ColorPickerComponent]
})
export class AppModule {}

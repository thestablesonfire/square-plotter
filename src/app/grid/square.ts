import { Location } from './location';

export class Square {
    location: Location;
    color: string;

    constructor(x, y, color) {
        this.location = new Location(x, y);
        this.color = color;
    }

    getSurroundingSquares() {
        return [
            new Location(this.location.x + 1, this.location.y),
            new Location(this.location.x - 1, this.location.y),
            new Location(this.location.x, this.location.y + 1),
            new Location(this.location.x, this.location.y - 1)
        ];
    }
}

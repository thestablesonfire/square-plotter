import { Square } from './square';
import { Location } from './location';

export class Grid {
    private grid: Square[][];
    squareWidth: number;
    gridWidth: number;

    constructor(public width: number, public height: number, color: string) {
        this.squareWidth = 13;
        this.gridWidth = 1;
        this.grid = [];
        for (let i = 0; i < this.height; i++) {
            this.grid[i] = [];
            for (let j = 0; j < this.width; j++) {
                this.grid[i][j] = new Square(j, i, color);
            }
        }
    }

    getGrid() {
        return this.grid;
    }

    locationExists(location: Location): boolean {
        const xCheck = (location.x >= 0 && location.x < this.width);
        const yCheck = (location.y >= 0 && location.y < this.height);

        return xCheck && yCheck;
    }
}

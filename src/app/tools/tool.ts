import { Square } from '../grid/square';
import {ActiveColorService} from '../color-picker/active-color.service';
import { Grid } from '../grid/grid';

export class Tool {
    constructor(public name: string, public icon: string, protected activeColorService: ActiveColorService) { }

    fill(square: Square, grid: Grid): void {
        square.color = this.activeColorService.activeColor;
    }
}

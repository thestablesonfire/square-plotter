import { Tool } from './tool';
import { Grid } from '../grid/grid';
import { Square } from '../grid/square';
import { ActiveColorService } from '../color-picker/active-color.service';

export class Eraser extends Tool {

    constructor(activeColorService: ActiveColorService) {
        super('Eraser', '&#x0072;', activeColorService);
    }

    fill(square: Square, grid: Grid) {
        square.color = this.activeColorService.emptyColor;
    }
}

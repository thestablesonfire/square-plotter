import { ActiveColorService } from '../color-picker/active-color.service';
import {Tool} from './tool';
import {Square} from '../grid/square';
import {Grid} from '../grid/grid';

export class Bucket extends Tool {
    colorToBeReplaced: string;

    constructor(activeColorService: ActiveColorService) {
        super('Bucket', '&#9639;', activeColorService);
    }

    fill(square: Square, grid: Grid): void {
        const queue = [square];
        let active;
        const map = new AddedMap(grid.width, grid.height);
        this.colorToBeReplaced = square.color;

        do {
            active = queue.pop();
            map.map[active.location.y][active.location.x] = true;
            active.color = this.activeColorService.activeColor;
            const surrounding = active.getSurroundingSquares();

            for (let i = 0; i < surrounding.length; i++) {
                if (grid.locationExists(surrounding[i])) {
                    const x = surrounding[i].x;
                    const y = surrounding[i].y;
                    const testSquare = grid.getGrid()[y][x];
                    if (!map.map[y][x] && this.squareIsEligibleToFill(testSquare)) {
                        queue.push(testSquare);
                        map.map[y][x] = true;
                    }
                }
            }
        } while (queue.length);
    }

    squareIsEligibleToFill(square: Square): boolean {
        return square.color === this.colorToBeReplaced;
    }
}

class AddedMap {
    map: boolean[][];
    constructor(public width: number, public height: number) {
        this.map = [];
        for (let i = 0; i < height; i++) {
            this.map[i] = [];
            for (let j = 0; j < width; j++) {
                this.map[i][j] = false;
            }
        }
    }
}

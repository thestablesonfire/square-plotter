import { Square } from '../grid/square';
import { Tool } from './tool';
import { ActiveColorService } from '../color-picker/active-color.service';

export class Pencil extends Tool {
    constructor(private activeColorSerice: ActiveColorService) {
        super('Pencil', '&#9998;', activeColorSerice);
    }
}

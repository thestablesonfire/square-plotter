import { TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ActiveColorService } from './color-picker/active-color.service';
import { PlotterComponent } from './plotter/plotter.component';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { ToolPickerComponent } from './tool-picker/tool-picker.component';
import { ActiveToolService } from './tool-picker/active-tool.service';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                PlotterComponent,
                ColorPickerComponent,
                ToolPickerComponent
            ],
            imports: [FormsModule],
            providers: [ActiveColorService, ActiveToolService]
        }).compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it(`should have as title 'Square Plotter'`, async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('Square Plotter');
    }));
});

import { Injectable } from '@angular/core';

import { Tool } from '../tools/tool';
import { Pencil } from '../tools/pencil';
import { ActiveColorService } from '../color-picker/active-color.service';

@Injectable()
export class ActiveToolService {
  activeTool: Tool;

  constructor(private activeColorService: ActiveColorService) {
    this.activeTool = new Pencil(activeColorService);
  }
  setActiveTool(tool) {
    this.activeTool = tool;
  }
}

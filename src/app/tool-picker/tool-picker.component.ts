import { Component } from '@angular/core';

import { Tool } from '../tools/tool';
import { Pencil } from '../tools/pencil';
import { Bucket } from '../tools/bucket';
import { Eraser } from '../tools/eraser';
import { ActiveColorService } from '../color-picker/active-color.service';
import { ActiveToolService } from './active-tool.service';

@Component({
  selector: 'app-tool-picker',
  templateUrl: './tool-picker.component.html',
  styleUrls: ['./tool-picker.component.scss']
})
export class ToolPickerComponent {
  tools: Tool[];
  activeTool: Tool;

  constructor(private activeColorService: ActiveColorService, private activeToolService: ActiveToolService) {
    this.tools = [new Pencil(activeColorService), new Bucket(activeColorService), new Eraser(activeColorService)];
      this.activeToolService.activeTool = this.tools[0];
      this.activeTool = this.tools[0];
  }

  setActiveTool(tool): void {
    this.activeTool = tool;
    this.activeToolService.setActiveTool(tool);
  }
}

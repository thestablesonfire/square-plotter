import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolPickerComponent } from './tool-picker.component';
import {ActiveColorService} from '../color-picker/active-color.service';
import {ActiveToolService} from './active-tool.service';

describe('ToolPickerComponent', () => {
  let component: ToolPickerComponent;
  let fixture: ComponentFixture<ToolPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolPickerComponent ],
        providers: [ ActiveColorService, ActiveToolService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

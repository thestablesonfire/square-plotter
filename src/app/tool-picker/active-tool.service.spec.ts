import { TestBed, inject } from '@angular/core/testing';

import { ActiveToolService } from './active-tool.service';
import { ActiveColorService } from '../color-picker/active-color.service';

describe('ActiveToolService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActiveToolService, ActiveColorService]
    });
  });

  it('should be created', inject([ActiveToolService], (service: ActiveToolService) => {
    expect(service).toBeTruthy();
  }));
});

import {Component, ElementRef} from '@angular/core';

import { ActiveColorService } from '../color-picker/active-color.service';
import { ActiveToolService } from '../tool-picker/active-tool.service';
import { Grid } from '../grid/grid';

@Component({
    selector: 'app-plotter',
    templateUrl: './plotter.component.html',
    styleUrls: ['./plotter.component.scss']
})
export class PlotterComponent {
    grid: Grid;
    mouseDown = false;
    displayGrid = true;
    imageSrc = '';
    isDirty: boolean;

    constructor(private activeColorService: ActiveColorService, private activeToolService: ActiveToolService) {
        this.newImage();
    }

    fillSquare(square) {
        this.activeToolService.activeTool.fill(square, this.grid);
        this.isDirty = true;
    }

    toggleChecked(square) {
        if (this.mouseDown) {
            this.activeToolService.activeTool.fill(square, this.grid);
        }
    }

    getGridWidth() {
        let width = this.displayGrid
            ? (this.grid.gridWidth * this.grid.width) + 1
            : 0;

        width += this.grid.width * this.grid.squareWidth;

        return width;
    }

    showPlotter() {
        this.imageSrc = '';
    }

    newImage() {
        this.imageSrc = '';
        this.grid = new Grid(25, 25, this.activeColorService.emptyColor);
        this.isDirty = false;
    }

    saveImage() {
        const canvas = <HTMLCanvasElement> document.getElementById('saverCanvas');
        const context: CanvasRenderingContext2D = canvas.getContext('2d');
        const grid = this.grid.getGrid();
        const squareWidth = this.grid.squareWidth;

        canvas.width = this.grid.squareWidth * this.grid.width;
        canvas.height = this.grid.squareWidth * this.grid.height;

        for (const row of grid) {
            for (const square of row) {
                this.drawSquare(context, square, squareWidth);
            }
        }

        this.cropImageFromCanvas(context, canvas);
    }

    drawSquare(context, square, width) {
        const startX = square.location.x * width;
        const startY = square.location.y * width;

        context.fillStyle = square.color;
        context.fillRect(startX, startY, width, width);
        context.stroke();
    }

    setMouseClick(mouseDown) {
        this.mouseDown = mouseDown;
    }

    cropImageFromCanvas(ctx, canvas) {

        let w = canvas.width,
            h = canvas.height,
            x, y, index;

        const pix = {x: [], y: []},
            imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

        for (y = 0; y < h; y++) {
            for (x = 0; x < w; x++) {
                index = (y * w + x) * 4;
                if (imageData.data[index + 3] > 0) {

                    pix.x.push(x);
                    pix.y.push(y);

                }
            }
        }
        pix.x.sort(function(a, b){ return a - b; });
        pix.y.sort(function(a, b){ return a - b; });
        const n = pix.x.length - 1;

        w = pix.x[n] - pix.x[0];
        h = pix.y[n] - pix.y[0];
        const cut = ctx.getImageData(pix.x[0], pix.y[0], w, h);

        canvas.width = w;
        canvas.height = h;
        ctx.putImageData(cut, 0, 0);

        this.imageSrc = canvas.toDataURL('image/png');
    }
}
